# TrackTracks

Analyze how your music affects your exercises.

## Run

```bash
docker compose up -d
celery -A spotirun.celery_config.app worker --loglevel=info
celery -A spotirun.celery_config.app flower --port=5555
celery -A spotirun.celery_config.app beat
```

Start FastAPI server in IDE or with:
```bash
uvicorn spotirun.main:app --reload
```


## Usage

1. Add Spotify Access Token at http://localhost:8090/spotify/login
