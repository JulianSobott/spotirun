"""add shared_activities table

Revision ID: 23edff11d350
Revises: ecd05ef2615b
Create Date: 2024-07-11 21:34:04.527357

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "23edff11d350"
down_revision = "ecd05ef2615b"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "shared_activities",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("user_id", sa.String, nullable=False),
        sa.Column(
            "activity_id",
            sa.Integer,
            sa.ForeignKey("user_activities.activity_id"),
            nullable=False,
        ),
        sa.Column("token", sa.String, nullable=False),
        sa.UniqueConstraint("user_id", "activity_id", name="user_id_activity_id"),
        sa.UniqueConstraint("activity_id", name="activity_id"),
        sa.UniqueConstraint("token", name="token"),
    )


def downgrade() -> None:
    op.drop_table("shared_activities")
