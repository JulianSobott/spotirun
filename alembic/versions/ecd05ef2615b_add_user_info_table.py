"""add user info table

Revision ID: ecd05ef2615b
Revises: a5bbf1076863
Create Date: 2024-06-08 18:15:05.107297

"""
import asyncio

import sqlalchemy as sa
from alembic import op

import spotirun.strava.auth
from spotirun.oauth2_helper import get_users, Applications, add_user_info
from spotirun.spotify.auth import get_user_info as get_spotify_user_info

# revision identifiers, used by Alembic.
revision = "ecd05ef2615b"
down_revision = "a5bbf1076863"
branch_labels = None
depends_on = None


def run_async(func, *args, **kwargs):
    loop = asyncio.get_event_loop()
    if loop.is_running():
        # If an event loop is already running, we can't start a new one, so we use create_task
        return loop.create_task(func(*args, **kwargs))
    else:
        return loop.run_until_complete(func(*args, **kwargs))


def upgrade() -> None:
    op.create_table(
        "user_info",
        sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
        sa.Column("app", sa.String(50), nullable=False),
        sa.Column("user_id", sa.String(50), nullable=False),
        sa.Column("user_info", sa.JSON, nullable=False),
        sa.UniqueConstraint("app", "user_id", name="app_user_id"),
    )
    # iterate over all users and add their user info to the user_info table
    users = run_async(get_users, Applications.spotify)
    for user in users:
        user_info = run_async(get_spotify_user_info, user)
        run_async(add_user_info, Applications.spotify, user, user_info)
        client = run_async(spotirun.strava.auth.authenticate, user)
        athlete = client.get_athlete()
        run_async(add_user_info, Applications.strava, user, athlete.dict())


def downgrade() -> None:
    op.drop_table("user_info")
