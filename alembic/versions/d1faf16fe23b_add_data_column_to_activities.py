"""add data column to activities

Revision ID: d1faf16fe23b
Revises: 23edff11d350
Create Date: 2024-07-25 15:34:30.001841

"""
import asyncio
import json
import os
from pathlib import Path

import sqlalchemy as sa
from alembic import op
from stravalib.model import Activity

from spotirun.strava.core import extract_activity_data

# revision identifiers, used by Alembic.
revision = "d1faf16fe23b"
down_revision = "23edff11d350"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "user_activities",
        sa.Column("data", sa.Text, nullable=False, server_default="{}"),
    )
    connection = op.get_bind()

    # Reflect the table from the database
    metadata = sa.MetaData()
    activities_table = sa.Table("user_activities", metadata, autoload_with=connection)

    data_path = Path(os.environ.get("SPOTIRUN_DATA_PATH", "data"))
    for folder in (data_path / "activities").iterdir():
        try:
            with open(folder / "activity.json") as f:
                activity = json.load(f)
                res = asyncio.run(extract_activity_data(Activity.parse_obj(activity)))
                connection.execute(
                    activities_table.update()
                    .where(activities_table.c.activity_id == activity["id"])
                    .values(data=json.dumps(res.dict()))
                )
        except Exception as e:
            print(f"Error processing {folder}: {e}")


def downgrade() -> None:
    op.drop_column("user_activities", "data")
