"""create initial tables

Revision ID: a5bbf1076863
Revises: c60fdb9180cb
Create Date: 2023-07-18 21:45:47.457814

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a5bbf1076863"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "tokens",
        sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
        sa.Column("app", sa.String(50), nullable=False),
        sa.Column("user_id", sa.String(50), nullable=False),
        sa.Column("access_token", sa.String(255), nullable=False),
        sa.Column("refresh_token", sa.String(255), nullable=False),
        sa.Column("expires_at", sa.DateTime, nullable=False),
        sa.UniqueConstraint("app", "user_id", name="app_user_id"),
    )
    op.create_table(
        "last_imports",
        sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
        sa.Column("user_id", sa.String(50), nullable=False),
        sa.Column("last_import", sa.DateTime, nullable=False),
        sa.UniqueConstraint("user_id", name="user_id"),
    )
    op.create_table(
        "played_songs",
        sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
        sa.Column("user_id", sa.String(50), nullable=False),
        sa.Column("played_at", sa.DateTime, nullable=False),
        sa.Column("song_id", sa.String(50), nullable=False),
        sa.Column("song_name", sa.String(255), nullable=False),
        sa.Column("artist_name", sa.String(255), nullable=False),
        sa.Column("album_name", sa.String(255), nullable=False),
        sa.Column("album_image_url", sa.String(255), nullable=False),
        sa.UniqueConstraint("user_id", "played_at", name="user_id_played_at"),
    )
    op.create_table(
        "user_activities",
        sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
        sa.Column("user_id", sa.String(50), nullable=False),
        sa.Column("activity_id", sa.INTEGER, nullable=False),
        sa.Column("activity_name", sa.String(255), nullable=False),
        sa.Column("start_date", sa.DateTime, nullable=False),
        sa.UniqueConstraint("user_id", "activity_id", name="user_id_activity_id"),
    )
    op.create_table(
        "user_athletes",
        sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
        sa.Column("user_id", sa.String(50), nullable=False),
        sa.Column("athlete_id", sa.INTEGER, nullable=False),
        sa.Column("athlete_name", sa.String(255), nullable=False),
        sa.UniqueConstraint("user_id", "athlete_id", name="user_id_athlete_id"),
    )


def downgrade() -> None:
    op.drop_table("tokens")
    op.drop_table("last_imports")
    op.drop_table("played_songs")
    op.drop_table("user_activities")
    op.drop_table("user_athletes")
