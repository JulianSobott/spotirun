from textwrap import dedent

from openai import OpenAI
from stravalib.strava_model import DetailedActivity

from spotirun.db import save_songs_analysis, Analysis, load_songs_analysis

client = OpenAI()


async def get_songs_analysis(
    activity_id: int, songs: list[dict], activity: DetailedActivity
) -> Analysis:
    if len(songs) == 0:
        return Analysis(facts=[], tags=[], title="Sound of Silence")
    if analysis := await load_songs_analysis(activity_id):
        return analysis
    analysis = await analyse_songs(songs, activity)
    await save_songs_analysis(activity_id, analysis)
    return analysis


async def analyse_songs(songs: list[dict], activity: DetailedActivity) -> Analysis:
    # spotify deprecated the audio features endpoint, so we must use empty data
    # tags, facts, title = await asyncio.gather(
    #     _generate_tags(songs), _generate_facts(songs), _generate_title(songs, activity)
    # )
    title = await _generate_title(songs, activity)
    return Analysis(facts=[], tags=[], title=title)


async def _generate_facts(songs: list[dict]) -> list[str]:
    songs_text = ""
    for song in songs:
        song_text = (
            f"danceability: {song['danceability']} "
            f"energy: {song['energy']} "
            f"instrumentalness: {song['instrumentalness']} "
            f"speechiness: {song['speechiness']} "
            f"tempo: {song['tempo']} "
            f"valence: {song['valence']}"
        )
        songs_text += song_text + "\n"

    prompt = dedent(
        """\
        I want you to summarize my songs that I listened to during my sport activity. 

        ## Attributes
        danceability: Danceability describes how suitable a track is for dancing based on a combination of musical elements including tempo, rhythm stability, beat strength, and overall regularity. A value of 0.0 is least danceable and 1.0 is most danceable.
        instrumentalness: Predicts whether a track contains no vocals. "Ooh" and "aah" sounds are treated as instrumental in this context. Rap or spoken word tracks are clearly "vocal". The closer the instrumentalness value is to 1.0, the greater likelihood the track contains no vocal content. Values above 0.5 are intended to represent instrumental tracks, but confidence is higher as the value approaches 1.0.
        energy: Energy is a measure from 0.0 to 1.0 and represents a perceptual measure of intensity and activity. Typically, energetic tracks feel fast, loud, and noisy. For example, death metal has high energy, while a Bach prelude scores low on the scale. Perceptual features contributing to this attribute include dynamic range, perceived loudness, timbre, onset rate, and general entropy.
        speechiness: Speechiness detects the presence of spoken words in a track. The more exclusively speech-like the recording (e.g. talk show, audio book, poetry), the closer to 1.0 the attribute value. Values above 0.66 describe tracks that are probably made entirely of spoken words. Values between 0.33 and 0.66 describe tracks that may contain both music and speech, either in sections or layered, including such cases as rap music. Values below 0.33 most likely represent music and other non-speech-like tracks.
        tempo: The overall estimated tempo of a track in beats per minute (BPM). In musical terminology, tempo is the speed or pace of a given piece and derives directly from the average beat duration.
        valence: A measure from 0.0 to 1.0 describing the musical positiveness conveyed by a track. Tracks with high valence sound more positive (e.g. happy, cheerful, euphoric), while tracks with low valence sound more negative (e.g. sad, depressed, angry).

        ## Output

        Output 4 key facts that summarize this activity.
        Each fact should only be one sentence and output in a new line.
        These facts are displayed in an app below the activity.
        Instead of telling numbers, interpret them.

        ## Output Format
        <fact 1>
        <fact 2>
        <fact 3>
        <fact 4>
        """
    )
    response = client.chat.completions.create(
        messages=[
            {"role": "system", "content": prompt},
            {
                "role": "user",
                "content": songs_text,
            },
        ],
        model="gpt-4o-mini",
    )
    message = response.choices[0].message.content
    lines = message.split("\n")
    return lines


async def _generate_tags(songs: list[dict]) -> list[str]:
    songs_text = ""
    for song in songs:
        song_text = (
            f"danceability: {song['danceability']} "
            f"energy: {song['energy']} "
            f"instrumentalness: {song['instrumentalness']} "
            f"speechiness: {song['speechiness']} "
            f"tempo: {song['tempo']} "
            f"valence: {song['valence']}"
        )
        songs_text += song_text + "\n"

    prompt = dedent(
        """\
        I want you to output 3 tags that describe the music I listened to during my activity.
        These are the attributes of the songs:

        ## Attributes
        danceability: Danceability describes how suitable a track is for dancing based on a combination of musical elements including tempo, rhythm stability, beat strength, and overall regularity. A value of 0.0 is least danceable and 1.0 is most danceable.
        instrumentalness: Predicts whether a track contains no vocals. "Ooh" and "aah" sounds are treated as instrumental in this context. Rap or spoken word tracks are clearly "vocal". The closer the instrumentalness value is to 1.0, the greater likelihood the track contains no vocal content. Values above 0.5 are intended to represent instrumental tracks, but confidence is higher as the value approaches 1.0.
        energy: Energy is a measure from 0.0 to 1.0 and represents a perceptual measure of intensity and activity. Typically, energetic tracks feel fast, loud, and noisy. For example, death metal has high energy, while a Bach prelude scores low on the scale. Perceptual features contributing to this attribute include dynamic range, perceived loudness, timbre, onset rate, and general entropy.
        speechiness: Speechiness detects the presence of spoken words in a track. The more exclusively speech-like the recording (e.g. talk show, audio book, poetry), the closer to 1.0 the attribute value. Values above 0.66 describe tracks that are probably made entirely of spoken words. Values between 0.33 and 0.66 describe tracks that may contain both music and speech, either in sections or layered, including such cases as rap music. Values below 0.33 most likely represent music and other non-speech-like tracks.
        tempo: The overall estimated tempo of a track in beats per minute (BPM). In musical terminology, tempo is the speed or pace of a given piece and derives directly from the average beat duration.
        valence: A measure from 0.0 to 1.0 describing the musical positiveness conveyed by a track. Tracks with high valence sound more positive (e.g. happy, cheerful, euphoric), while tracks with low valence sound more negative (e.g. sad, depressed, angry).
        
        ## Output
        
        Output 3 tags, like "energetic", "uplifting", "fast-paced", that describe the music of the activity.
        
        ## Output Format
        <tag 1>, <tag 2>, <tag 3>
        """
    )
    response = client.chat.completions.create(
        messages=[
            {"role": "system", "content": prompt},
            {
                "role": "user",
                "content": songs_text,
            },
        ],
        model="gpt-4o-mini",
    )
    message = response.choices[0].message.content
    return message.split(", ")


async def _generate_title(songs: list[dict], activity: DetailedActivity) -> str:
    songs_text = ""
    for song in songs:
        artists = ", ".join([artist["name"] for artist in song["artists"]])
        song_text = f"song: '{song['name']}' artists: '{artists}' album: '{song['album']['name']}' "
        songs_text += song_text + "\n"
    prompt = dedent(
        """\
        I want you to generate a title for my sport activity based on the songs I listened to.
        
        ## Output
        
        Output a title for this run, that is 1-4 words long, which is suitable for Strava.
        For example: Master of Puppets Afternoon Run
        
        ## Output Format
        <title>
        """
    )
    response = client.chat.completions.create(
        messages=[
            {"role": "system", "content": prompt},
            {
                "role": "user",
                "content": songs_text
                + f"\n\nType of the activity: {activity.type}\nOriginal title: {activity.name}",
            },
        ],
        model="gpt-4o-mini",
    )
    return response.choices[0].message.content.strip()
