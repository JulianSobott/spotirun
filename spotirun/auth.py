"""Python Flask WebApp Auth0 integration example
"""

from os import environ as env
from urllib.parse import quote_plus, urlencode

from authlib.integrations.starlette_client import OAuth
from fastapi import APIRouter, Request, HTTPException
from pydantic import BaseModel, Field
from starlette import status
from starlette.responses import RedirectResponse

oauth: OAuth | None = None

router = APIRouter()


def setup_oauth():
    global oauth
    oauth = OAuth()

    oauth.register(
        "auth0",
        client_id=env.get("AUTH0_CLIENT_ID"),
        client_secret=env.get("AUTH0_CLIENT_SECRET"),
        client_kwargs={
            "scope": "openid profile email",
        },
        server_metadata_url=f'https://{env.get("AUTH0_DOMAIN")}/.well-known/openid-configuration',
    )


class Auth0User(BaseModel):
    sid: str
    id: str = Field(alias="sub")
    given_name: str | None = None
    family_name: str | None = None
    nickname: str | None = None
    name: str | None = None
    picture: str | None = None
    locale: str | None = None
    updated_at: str | None = None
    email: str | None = None
    email_verified: bool | None = None
    iss: str | None = None
    aud: str | None = None
    iat: int | None = None
    exp: int | None = None
    nonce: str | None = None


async def current_user(request: Request) -> Auth0User:
    user = request.session.get("user")
    if not user:
        request.url_for("login")
        raise HTTPException(
            status_code=status.HTTP_307_TEMPORARY_REDIRECT,
            headers={"Location": str(request.url_for("login"))},
        )
    user = Auth0User(**user["userinfo"])
    return user


@router.route("/callback", methods=["GET", "POST"])
async def callback(request: Request):
    token = await oauth.auth0.authorize_access_token(request)
    request.session["user"] = token
    return RedirectResponse(url=request.url_for("home"))


@router.get("/login")
async def login(request: Request):
    redirect_uri = request.url_for("callback")
    return await oauth.auth0.authorize_redirect(request, redirect_uri)


@router.get("/logout")
def logout(request: Request):
    request.session.clear()
    return_to = request.url_for("home")
    logout_url = f"https://{env.get('AUTH0_DOMAIN')}/v2/logout?" + urlencode(
        {
            "returnTo": return_to,
            "client_id": env.get("AUTH0_CLIENT_ID"),
        },
        quote_via=quote_plus,
    )
    return RedirectResponse(url=logout_url)
