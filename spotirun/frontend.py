import asyncio
import logging
import secrets
from datetime import datetime, timedelta

from dateutil.parser import parse
from fastapi import APIRouter, Depends, Request
from fastapi.responses import FileResponse
from fastapi.templating import Jinja2Templates
from starlette.responses import Response, RedirectResponse

from spotirun.analysis import get_songs_analysis
from spotirun.auth import current_user, Auth0User
from spotirun.db import (
    get_shared_token,
    save_shared_token,
    get_shared_activity,
    get_tags_for_activities,
)
from spotirun.oauth2_helper import get_user_info, Applications, get_token
from spotirun.spotify.core import get_songs_information
from spotirun.spotify.db import get_songs_between
from spotirun.strava.core import (
    strava_recent_activities,
    split_by_indices,
    analyse_split,
    unit_filter,
    unit_name_filter,
)
from spotirun.strava.db import get_activity, get_streams, activities_dir

router = APIRouter()


templates = Jinja2Templates(directory="templates")
templates.env.filters["unit"] = unit_filter
templates.env.filters["unit_name"] = unit_name_filter

logger = logging.getLogger(__name__)


@router.get("/home")
async def home(request: Request, user: Auth0User = Depends(current_user)):
    spotify_authenticated = (await get_token(Applications.spotify, user.id)) is not None
    strava_authenticated = (await get_token(Applications.strava, user.id)) is not None
    activities = await strava_recent_activities(user.id)
    tags = await get_tags_for_activities([activity.id for activity in activities])
    return templates.TemplateResponse(
        "app/home.html.j2",
        {
            "request": request,
            "user": user,
            "spotify_authenticated": spotify_authenticated,
            "strava_authenticated": strava_authenticated,
            "activities": activities,
            "tags": tags,
        },
    )


async def get_analytics(
    request: Request, activity_id: int, user_id: str, is_shared_activity=False
):
    activity = await get_activity(activity_id)
    streams = await get_streams(activity_id)

    if "latlng" not in streams:  # training without GPS, e.g., a yoga session
        return templates.TemplateResponse(
            "app/no_positions.html.j2",
            {
                "request": request,
                "activity": activity,
            },
        )
    polyline_coords = streams["latlng"]
    velocities = streams["velocity_smooth"]
    times = streams["time"]
    start = activity.start_date
    if isinstance(start, str):
        start = parse(start)
    end = start + timedelta(seconds=activity.elapsed_time)
    # TODO: fix, when songs are immediately imported after a run
    # simulate start and end as if they ended just now in UTC
    # end = datetime.now()
    # end = end.astimezone(tzutc())
    # start = end - activity.elapsed_time

    logger.info(f"analytics: {start=} {end=}")

    songs_raw = await get_songs_between(user_id, start, end)
    song_ids = [song["song_id"] for song in songs_raw]
    songs_info = await get_songs_information(user_id, song_ids)
    logger.info(f"analytics: {len(songs_raw)=}")
    songs = []
    for i, song in enumerate(songs_raw):
        # 2023-07-06T23:34:55.141Z
        played_at = parse(song["played_at"])
        song_info = songs_info[i]
        played_duration = song_info["duration_ms"] // 1000
        played_start = played_at - timedelta(seconds=played_duration)
        stream_indices_of_song = get_stream_indices_of_song(
            played_start, played_duration, times, start
        )
        if stream_indices_of_song[1] - stream_indices_of_song[0] > 1:
            stream_splits = split_by_indices(stream_indices_of_song, streams)
            try:
                song_split = analyse_split(stream_splits)
            except Exception as e:
                logger.error(f"Error analysing split: {e}")
                song_split = {}
            songs.append(
                {
                    "icon_url": song["album_image_url"],
                    "url": song_info["external_urls"]["spotify"],
                    "name": song["song_name"],
                    "artist": song["artist_name"],
                    "album": song["album_name"],
                    "position_indices": stream_indices_of_song,
                    "split": song_split,
                }
            )

    analysis = await get_songs_analysis(activity_id, songs_info, activity)
    return templates.TemplateResponse(
        request,
        "app/main.html.j2",
        {
            "activity": activity,
            "polyline_coords": polyline_coords,
            "velocities": velocities,
            "songs": songs,
            "shared_activity": is_shared_activity,
            "analysis": analysis.dict(),
        },
    )


@router.get("/activity/{activity_id}/images/{image}")
async def activity_image(activity_id: int, image: str):
    # prevent directory traversal
    if "/" in image:
        return Response("Invalid image path", status_code=400)
    logger.info(activities_dir / str(activity_id) / image)
    return FileResponse(activities_dir / str(activity_id) / image)


@router.get("/analytics/{activity_id}")
async def analytics(
    request: Request,
    activity_id: int,
    user: Auth0User = Depends(current_user),
):
    return await get_analytics(request, activity_id, user.id)


@router.get("/share/{token}")
async def shared_activity(token: str, request: Request):
    response = await get_shared_activity(token)
    if not response:
        return Response("Activity not found", status_code=404)
    user_id, activity_id = response
    return await get_analytics(request, activity_id, user_id, is_shared_activity=True)


@router.post("/share/{activity_id}")
async def share_activity(
    activity_id: int,
    user: Auth0User = Depends(current_user),
):
    if token := await get_shared_token(activity_id):
        return {"message": "Activity already shared", "token": token}
    share_token = secrets.token_urlsafe(16)
    await save_shared_token(activity_id, user.id, share_token)
    return {"message": "Activity shared", "token": share_token}


@router.get("/setup")
async def setup_page(request: Request, user: Auth0User = Depends(current_user)):
    spotify_data_future = get_user_info(Applications.spotify, user.id)
    strava_data_future = get_user_info(Applications.strava, user.id)
    spotify_data, strava_data = await asyncio.gather(
        spotify_data_future, strava_data_future
    )
    connected_services = {}
    if spotify_data:
        connected_services["spotify"] = {"username": spotify_data["display_name"]}
    if strava_data:
        connected_services["strava"] = {"username": strava_data["username"]}
    all_services_connected = spotify_data and strava_data

    return templates.TemplateResponse(
        request=request,
        name="app/setup.html.j2",
        context={
            "connected_services": connected_services,
            "all_services_connected": all_services_connected,
        },
    )


@router.get("/profile")
async def profile(request: Request, user: Auth0User = Depends(current_user)):
    return templates.TemplateResponse(request=request, name="app/profile.html.j2")


@router.get("/profile/picture")
async def profile_picture(user: Auth0User = Depends(current_user)):
    data = await get_user_info(Applications.strava, user.id)
    image_url = data["profile_medium"]
    return RedirectResponse(url=image_url)


def get_stream_indices_of_song(
    played_at: datetime,
    played_duration: int,
    times: list[int],
    activity_start: datetime,
):
    """
    Returns the indices of the stream where the song was played.
    """
    logger.debug(
        f"get_stream_indices_of_song: {played_at.isoformat()=} {played_duration=} {activity_start.isoformat()=} {len(times)=}"
    )
    played_at_rel = played_at - activity_start
    played_at_seconds = int(played_at_rel.total_seconds())
    played_at_index_start = _get_index_of_closest_time(played_at_seconds, times)
    played_at_index_end = _get_index_of_closest_time(
        played_at_seconds + played_duration, times
    )
    logger.debug(
        f"get_stream_indices_of_song: {played_at_index_start=} {played_at_index_end=}"
    )
    return played_at_index_start, played_at_index_end


def _get_index_of_closest_time(time: int, times: list[int]):
    """
    Returns the index of the closest time in the list of times.
    """
    if time > times[-1]:
        return len(times) - 1
    try:
        index = times.index(time)
    except ValueError:
        # no exact match, find closest
        index = -1
        for i, t in enumerate(times):
            if t > time:
                index = i
                break
    if index == -1:
        raise ValueError("Could not find stream index for song")
    return index
