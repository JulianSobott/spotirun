import logging
from datetime import datetime

import tenacity
from authlib.integrations.httpx_client import AsyncOAuth2Client

from spotirun.spotify.auth import authenticate
from spotirun.spotify.db import (
    get_song_information,
    add_song_information,
)

logger = logging.getLogger(__name__)


class RateLimitError(Exception):
    pass


@tenacity.retry(
    stop=tenacity.stop_after_attempt(3),
    wait=tenacity.wait_random_exponential(multiplier=10, max=600),
    retry=tenacity.retry_if_exception_type(RateLimitError),
)
async def get_songs_after(
    after: datetime, client: AsyncOAuth2Client, num_songs: int = 30
):
    url = "https://api.spotify.com/v1/me/player/recently-played"
    params = {
        "limit": num_songs,  # Adjust the limit as per your needs
        "after": int(after.timestamp() * 1000),  # Convert start time to milliseconds
    }
    headers = {"Accept": "application/json", "Content-Type": "application/json"}
    response = await client.get(url, params=params, headers=headers)
    if response.status_code == 429:
        logger.warning("Rate limit exceeded")
        raise RateLimitError()
    response.raise_for_status()
    return response.json()


async def get_songs_information(user_id: str, ids: list[str]) -> list[dict]:
    if len(ids) == 0:
        return []
    songs, missing_ids = await _load_songs_information_from_cache(ids)
    if len(missing_ids) > 0:
        missing_songs = await _fetch_songs_from_spotify(user_id, missing_ids)
        missing_features = await _fetch_audio_features_from_spotify(
            user_id, missing_ids
        )
        for song, features in zip(missing_songs, missing_features):
            song.update(features)
        songs.extend(missing_songs)
        for song in missing_songs:
            await add_song_information(song["id"], song)
    return songs


async def _fetch_songs_from_spotify(user_id: str, ids: list[str]) -> list[dict]:
    client = await authenticate(user_id)
    url = "https://api.spotify.com/v1/tracks"
    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    all_tracks = []
    max_items_per_call = 50
    id_chunks = [
        ids[i : i + max_items_per_call] for i in range(0, len(ids), max_items_per_call)
    ]

    for chunk in id_chunks:
        params = {"ids": ",".join(chunk)}
        response = await client.get(url, params=params, headers=headers)
        response.raise_for_status()
        all_tracks.extend(response.json()["tracks"])

    return all_tracks


async def _load_songs_information_from_cache(
    ids: list[str],
) -> tuple[list[dict], list[str]]:
    songs = []
    missing_ids = []
    for song_id in ids:
        song = await get_song_information(song_id)
        if song is None:
            missing_ids.append(song_id)
            continue
        songs.append(song)
    return songs, missing_ids


async def _fetch_audio_features_from_spotify(
    user_id: str, ids: list[str]
) -> list[dict]:
    # API is deprecated so we must use empty data until we find an alternative
    return [{} for _ in ids]
    client = await authenticate(user_id)
    url = "https://api.spotify.com/v1/audio-features"
    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    all_features = []
    max_items_per_call = 100
    id_chunks = [
        ids[i : i + max_items_per_call] for i in range(0, len(ids), max_items_per_call)
    ]

    for chunk in id_chunks:
        params = {"ids": ",".join(chunk)}
        response = await client.get(url, params=params, headers=headers)
        response.raise_for_status()
        all_features.extend(response.json()["audio_features"])

    return all_features
