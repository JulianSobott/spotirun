import logging
import os
import secrets

from authlib.integrations.httpx_client import AsyncOAuth2Client
from authlib.integrations.starlette_client import OAuth
from dotenv import load_dotenv
from fastapi import APIRouter, Request, Depends
from starlette.responses import RedirectResponse

from spotirun.auth import current_user, Auth0User
from spotirun.oauth2_helper import (
    get_user_id_from_state,
    set_state_for_user,
    add_token,
    Applications,
    get_token,
    delete_token,
    add_user_info,
    delete_user_info,
)

load_dotenv()

logger = logging.getLogger(__name__)

spotify_client_id = os.environ.get("SPOTIFY_CLIENT_ID")
spotify_client_secret = os.environ.get("SPOTIFY_CLIENT_SECRET")

oauth = OAuth()
spotify_auth = oauth.register(
    name="spotify",
    client_id=spotify_client_id,
    client_secret=spotify_client_secret,
    authorize_url="https://accounts.spotify.com/authorize",
    access_token_url="https://accounts.spotify.com/api/token",
    refresh_token_url="https://accounts.spotify.com/api/token",
    api_base_url="https://api.spotify.com/v1/",
    client_kwargs={
        "scope": "user-read-private user-read-email user-read-recently-played"
    },
)

router = APIRouter()


@router.get("/login")
async def spotify_login(request: Request, user: Auth0User = Depends(current_user)):
    state = secrets.token_urlsafe(16)
    set_state_for_user(user.id, state)
    redirect_uri = request.url_for("spotify_authorized")
    return await spotify_auth.authorize_redirect(request, redirect_uri, state=state)


@router.get("/authorized", name="spotify_authorized")
async def spotify_authorized(request: Request):
    token = await spotify_auth.authorize_access_token(request)
    state = request.query_params.get("state")
    user = get_user_id_from_state(state)
    logger.info(f"User {user} authorized")
    await add_token(Applications.spotify, user, token)
    user_info = await get_user_info(user)
    await add_user_info(Applications.spotify, user, user_info)
    return RedirectResponse(url=request.url_for("setup_page"))


@router.get("/logout")
async def spotify_logout(request: Request, user: Auth0User = Depends(current_user)):
    await delete_token(Applications.spotify, user.id)
    await delete_user_info(Applications.spotify, user.id)
    return RedirectResponse(url=request.url_for("setup_page"))


def update_token(app: Applications, user_id: str):
    async def callback(token, **kwargs):
        await add_token(app, user_id, token)

    return callback


async def authenticate(user_id: str):
    token = await get_token(Applications.spotify, user_id)
    if token is None:
        raise Exception(f"No token found for user {user_id}")
    session = AsyncOAuth2Client(
        client_id=spotify_client_id,
        client_secret=spotify_client_secret,
        token=token,
        token_endpoint="https://accounts.spotify.com/api/token",
        update_token=update_token(Applications.spotify, user_id),
    )
    return session


async def get_user_info(user_id: str) -> dict:
    client = await authenticate(user_id)
    url = "https://api.spotify.com/v1/me"
    response = await client.get(url)
    response.raise_for_status()
    return response.json()
