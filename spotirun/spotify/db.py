import json
import logging
from datetime import datetime

from spotirun.db import db, redis_db

logger = logging.getLogger(__name__)


song_prefix = "spotirun:song:"


async def get_last_import(user_id: str) -> datetime | None:
    row = await db.fetch_one(
        "SELECT last_import FROM last_imports WHERE user_id = :user_id",
        {"user_id": user_id},
    )
    if row is None:
        return None
    return datetime.fromisoformat(row["last_import"])


async def save_songs(user_id: str, songs: dict) -> None:
    await db.execute_many(
        "INSERT INTO played_songs (user_id, played_at, song_id, song_name, artist_name, album_name, album_image_url) "
        "VALUES (:user_id, :played_at, :song_id, :song_name, :artist_name, :album_name, :album_image_url)"
        "ON CONFLICT (user_id, played_at) DO NOTHING",
        [
            {
                "user_id": user_id,
                "played_at": song["played_at"],
                "song_id": song["track"]["id"],
                "song_name": song["track"]["name"],
                "artist_name": ";".join([a["name"] for a in song["track"]["artists"]]),
                "album_name": song["track"]["album"]["name"],
                "album_image_url": song["track"]["album"]["images"][-1]["url"],
            }
            for song in reversed(
                songs["items"]
            )  # insert songs from first played to last (most recently) played
        ],
    )


async def set_last_import(user_id: str, last_import: datetime) -> None:
    await db.execute(
        "INSERT INTO last_imports (user_id, last_import) VALUES (:user_id, :last_import) "
        "ON CONFLICT (user_id) DO UPDATE SET last_import = :last_import",
        {"user_id": user_id, "last_import": last_import.isoformat()},
    )


async def get_songs_between(user_id: str, start: datetime, end: datetime) -> list[dict]:
    return await db.fetch_all(
        "SELECT * FROM played_songs WHERE user_id = :user_id AND played_at >= :start AND played_at <= :end",
        {"user_id": user_id, "start": start.isoformat(), "end": end.isoformat()},
    )


async def add_song_information(song_id: str, song: dict):
    redis_db.set(f"{song_prefix}{song_id}", json.dumps(song), ex=60 * 60 * 24 * 7)


async def get_song_information(song_id: str) -> dict | None:
    song = redis_db.get(f"{song_prefix}{song_id}")
    if song is None:
        return None
    return json.loads(song)
