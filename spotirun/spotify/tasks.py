import logging
from datetime import datetime, timedelta

from dateutil import parser
from dateutil.tz import tzlocal

from spotirun.oauth2_helper import get_users, Applications
from spotirun.spotify.auth import authenticate
from spotirun.spotify.core import get_songs_after
from spotirun.spotify.db import (
    get_last_import,
    save_songs,
    set_last_import,
)
from spotirun.utils import async_task

logger = logging.getLogger(__name__)


@async_task()
async def trigger_imports():
    users = await get_users(Applications.spotify)
    for user in users:
        import_recently_played.delay(user)


@async_task()
async def import_recently_played(user_id: str):
    session = await authenticate(user_id)
    logger.info(f"Importing recently played for user {user_id}")
    last_played_at_local = await get_last_import(user_id)
    if last_played_at_local is None:
        last_played_at_local = datetime.now() - timedelta(hours=2)
    import_after = last_played_at_local + timedelta(minutes=1)
    songs = await get_songs_after(import_after, session)
    if len(songs["items"]) == 0:
        logger.info(f"No songs to import for user {user_id}")
    else:
        logger.info(
            f"Fetched {len(songs['items'])} songs for user {user_id} after {import_after} "
            f"{songs['items'][0]['played_at']}"
        )
        await save_songs(user_id, songs)
        latest_played_at = songs["items"][0]["played_at"]
        latest_played_at_utc = parser.isoparse(latest_played_at)
        latest_played_at_local = latest_played_at_utc.astimezone(tzlocal())
        await set_last_import(user_id, latest_played_at_local)
    logger.info(f"Imported recently played for user {user_id}")
