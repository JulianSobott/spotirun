import os

from celery import Celery

app = Celery(
    "spotirun",
    broker=os.environ.get("CELERY_BROKER", "redis://localhost:6379/0"),
    backend=os.environ.get("CELERY_BACKEND", "redis://localhost:6379/0"),
    include=["spotirun.spotify.tasks"],
)

app.conf.beat_schedule = {
    "spotify-import": {
        "task": "spotirun.spotify.tasks.trigger_imports",
        "schedule": int(os.environ.get("SPOTIFY_IMPORT_INTERVAL", 120)),
    }
}
