import io
import json
import math
import os
from pathlib import Path

import numpy as np
import plotly.express as px

from spotirun.strava.db import activities_dir

mapbox_access_token = os.getenv("MAPBOX_ACCESS_TOKEN")


def render_route_to_image(
    lats: list[float], lons: list[float], destination_path: Path | io.BytesIO
):
    # Define the size of the image
    width = 1088
    height = 436

    # Calculate the bounding box
    min_lat, max_lat = min(lats), max(lats)
    min_lon, max_lon = min(lons), max(lons)

    # Calculate center of the bounding box
    center_lat = (min_lat + max_lat) / 2
    center_lon = (min_lon + max_lon) / 2

    margin = 0.005
    # Calculate the zoom level
    lat_range = max_lat - min_lat + margin
    lon_range = max_lon - min_lon + margin

    zoom_lat = np.log2(360 / lat_range)
    zoom_lon = np.log2(360 / lon_range)
    zoom = min(zoom_lat, zoom_lon)
    zoom = math.floor(zoom)

    # Create a plot
    px.set_mapbox_access_token(mapbox_access_token)
    fig = px.line_mapbox(
        lat=lats,
        lon=lons,
        center=dict(lat=center_lat, lon=center_lon),
        mapbox_style="open-street-map",
        zoom=zoom,
        width=width,
        height=height,
    )
    fig.update_layout(margin=dict(l=0, r=0, t=0, b=0))
    fig.write_image(destination_path)


if __name__ == "__main__":
    from tqdm import tqdm

    # Render all activities
    activities = list(activities_dir.iterdir())
    for activity_dir in tqdm(activities, desc="Rendering activities"):
        if not activity_dir.is_dir():
            continue
        if not (activity_dir / "latlng.json").exists():
            continue
        with open(activity_dir / "latlng.json") as f:
            route = json.load(f)
        latitudes, longitudes = zip(*route)
        render_route_to_image(latitudes, longitudes, activity_dir / "route.png")
