import io
import json
import logging
import os
from datetime import datetime
from pathlib import Path

from pydantic import BaseModel, Field
from stravalib.model import DetailedActivity as Activity, DetailedAthlete as Athlete

from spotirun.db import db

logger = logging.getLogger(__name__)

data_path = Path(os.environ.get("SPOTIRUN_DATA_PATH", "data"))
activities_dir = data_path / "activities"

activities_dir.mkdir(parents=True, exist_ok=True)


class Stat(BaseModel):
    name: str
    value: str


class ActivityData(BaseModel):
    statistics: list[Stat] = Field(default_factory=list)


class BasicActivity(BaseModel):
    id: int
    name: str
    start_date: datetime
    data: ActivityData


async def save_stream(activity_id: int, stream_type: str, data: list) -> None:
    activity_dir = activities_dir / str(activity_id)
    activity_dir.mkdir(parents=True, exist_ok=True)
    with open(activity_dir / f"{stream_type}.json", "w") as f:
        json.dump(data, f)


async def save_activity(activity_id: int, activity: Activity):
    activity_dir = activities_dir / str(activity_id)
    activity_dir.mkdir(parents=True, exist_ok=True)
    with open(activity_dir / "activity.json", "w") as f:
        f.write(activity.json())


async def save_activity_image(activity_id: int, image: io.BytesIO):
    activity_dir = activities_dir / str(activity_id)
    activity_dir.mkdir(parents=True, exist_ok=True)
    with open(activity_dir / "route.png", "wb") as f:
        f.write(image.getvalue())


async def add_user_activity(
    user_id: str,
    activity_id: int,
    activity_name: str,
    start_date: datetime,
    data: dict = None,
) -> None:
    data = data or {}
    await db.execute(
        "INSERT INTO user_activities (user_id, activity_id, activity_name, start_date, data) "
        "VALUES (:user_id, :activity_id, :activity_name, :start_date, :data) "
        "ON CONFLICT (user_id, activity_id) DO UPDATE SET "
        "activity_name=excluded.activity_name, start_date=excluded.start_date",
        {
            "user_id": user_id,
            "activity_id": activity_id,
            "activity_name": activity_name,
            "start_date": start_date.isoformat(),
            "data": json.dumps(data),
        },
    )


async def update_user_activity(activity_id: int, activity_name: str) -> None:
    await db.execute(
        "UPDATE user_activities SET activity_name=:activity_name "
        "WHERE activity_id=:activity_id",
        {
            "activity_id": activity_id,
            "activity_name": activity_name,
        },
    )


async def add_user_athlete(user_id: str, athlete: Athlete) -> None:
    await db.execute(
        "INSERT INTO user_athletes (user_id, athlete_id, athlete_name) "
        "VALUES (:user_id, :athlete_id, :athlete_name) "
        "ON CONFLICT (user_id, athlete_id) DO UPDATE SET "
        "athlete_name=excluded.athlete_name",
        {
            "user_id": user_id,
            "athlete_id": athlete.id,
            "athlete_name": athlete.username,
        },
    )


async def get_user_athlete(athlete_id: int) -> str | None:
    return await db.fetch_val(
        "SELECT user_id FROM user_athletes WHERE athlete_id = :athlete_id",
        {"athlete_id": athlete_id},
    )


async def get_activity(activity_id: int) -> Activity | None:
    activity_dir = activities_dir / str(activity_id)
    if not activity_dir.exists():
        return None
    with open(activity_dir / "activity.json", "r") as f:
        activity_json = json.load(f)
        return Activity.parse_obj(activity_json)


async def get_activities(user_id: str, limit: int = 10) -> list[BasicActivity]:
    rows = await db.fetch_all(
        "SELECT activity_id, activity_name, start_date, data FROM user_activities "
        "WHERE user_id = :user_id "
        "ORDER BY start_date DESC "
        "LIMIT :limit",
        {"user_id": user_id, "limit": limit},
    )
    activities = []
    for row in rows:
        # local_date: start_date=yyyy-mm-dd HH:MM:SS+00:00 to yyyy-mm-dd HH:MM:SS
        dt_with_tz = datetime.strptime(row["start_date"], "%Y-%m-%dT%H:%M:%S%z")
        dt_naive = dt_with_tz.replace(tzinfo=None)
        start_time = dt_naive.strftime("%Y-%m-%d %H:%M:%S")
        activity = BasicActivity(
            id=row["activity_id"],
            name=row["activity_name"],
            start_date=dt_naive,
            data=ActivityData.parse_raw(row["data"]),
        )
        activities.append(activity)
    return activities


async def get_streams(activity_id: int) -> dict[str, list]:
    activity_dir = activities_dir / str(activity_id)
    if not activity_dir.exists():
        return {}
    streams = {}
    for stream in activity_dir.glob("*.json"):
        if stream.stem == "activity":
            continue
        with open(stream, "r") as f:
            streams[stream.stem] = json.load(f)
    return streams
