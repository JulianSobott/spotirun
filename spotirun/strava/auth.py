import os
import secrets
from datetime import datetime

from fastapi import APIRouter, Request, Depends
from fastapi.responses import RedirectResponse
from stravalib.client import Client
from stravalib.util import limiter

from spotirun.auth import current_user, Auth0User
from spotirun.oauth2_helper import (
    set_state_for_user,
    get_user_id_from_state,
    add_token,
    Applications,
    get_token,
    delete_token,
    add_user_info,
    delete_user_info,
)
from spotirun.strava.db import add_user_athlete

strava_client_id = int(os.environ.get("STRAVA_CLIENT_ID") or 0)
strava_client_secret = os.environ.get("STRAVA_CLIENT_SECRET")
strava_verify_token = os.environ.get("STRAVA_VERIFY_TOKEN")

rate_limiter = limiter.DefaultRateLimiter()


router = APIRouter()


@router.get("/login")
async def strava_login(request: Request, user: Auth0User = Depends(current_user)):
    state = secrets.token_urlsafe(16)
    set_state_for_user(user.id, state)
    redirect_uri = str(request.url_for("strava_authorized"))
    client = Client(rate_limit_requests=True, rate_limiter=rate_limiter)
    authorize_url = client.authorization_url(
        client_id=strava_client_id,
        redirect_uri=redirect_uri,
        state=state,
        scope=["activity:read_all", "read"],
    )
    return RedirectResponse(authorize_url)


@router.get("/authorized")
async def strava_authorized(request: Request):
    code = request.query_params["code"]
    state = request.query_params["state"]
    user_id = get_user_id_from_state(state)
    client = Client(rate_limit_requests=True, rate_limiter=rate_limiter)
    token_response = client.exchange_code_for_token(
        client_id=strava_client_id, client_secret=strava_client_secret, code=code
    )
    await add_token(Applications.strava, user_id, token_response)
    athlete = client.get_athlete()
    await add_user_athlete(user_id, athlete)
    await add_user_info(Applications.strava, user_id, athlete.dict())
    return RedirectResponse(url=request.url_for("setup_page"))


@router.get("/logout")
async def strava_logout(request: Request, user: Auth0User = Depends(current_user)):
    await delete_token(Applications.strava, user.id)
    await delete_user_info(Applications.strava, user.id)
    return RedirectResponse(url=request.url_for("setup_page"))


# Create a subscription to receive updates when a new activity is created.
# Only needs to be created once for the application.
# Only here, if the URL of the website changes, the subscription must be updated.
"""
@router.get("/subscriptions")
async def strava_subscriptions(request: Request):
    client = Client(rate_limit_requests=True, rate_limiter=rate_limiter)
    subs = client.list_subscriptions(strava_client_id, strava_client_secret)
    try:
        sub = subs.next()
        if sub:
            client.delete_subscription(sub.id, strava_client_id, strava_client_secret)
    except StopIteration:
        pass
    client.create_subscription(
        client_id=strava_client_id,
        client_secret=strava_client_secret,
        callback_url="https://tracktracks.eu/strava/webhook",
        verify_token=strava_verify_token,
    )
    return client.list_subscriptions(strava_client_id, strava_client_secret).next()
"""


async def authenticate(user_id: str) -> Client:
    token = await get_token(Applications.strava, user_id)
    client = Client(rate_limit_requests=True, rate_limiter=rate_limiter)
    client.access_token = token["access_token"]
    client.refresh_token = token["refresh_token"]
    client.token_expires_at = token["expires_at"]
    if client.token_expires_at < int(datetime.now().timestamp()):
        refresh_response = client.refresh_access_token(
            client_id=strava_client_id,
            client_secret=strava_client_secret,
            refresh_token=client.refresh_token,
        )
        await add_token(Applications.strava, user_id, refresh_response)
    return client
