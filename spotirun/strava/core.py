import asyncio
import datetime
import io
import logging

from pint import Quantity
from stravalib import model
from stravalib.client import Client
from stravalib.model import DetailedActivity as Activity

from spotirun.strava.auth import authenticate
from spotirun.strava.db import (
    save_stream,
    save_activity,
    add_user_activity,
    get_user_athlete,
    get_activities,
    BasicActivity,
    update_user_activity,
    save_activity_image,
    ActivityData,
    Stat,
)
from spotirun.strava.renderer import render_route_to_image

logger = logging.getLogger(__name__)


async def import_user_activity(user_id, activity_id, client: Client):
    activity = await import_activity(activity_id, client)
    data = await extract_activity_data(activity)
    await add_user_activity(
        user_id, activity_id, activity.name, activity.start_date, data.dict()
    )
    logger.info(f"Imported activity {activity_id} for user {user_id}")


async def import_activity(activity_id: int, client: Client):
    stream_types = ["time", "latlng", "altitude", "heartrate", "velocity_smooth"]

    streams_loaded = client.get_activity_streams(
        activity_id, types=stream_types, resolution="medium"
    )
    stream_routines = []
    for stream in streams_loaded:
        cr = save_stream(activity_id, stream, streams_loaded[stream].data)
        stream_routines.append(cr)
        if stream == "latlng":
            lats, lons = zip(*streams_loaded[stream].data)
            image = io.BytesIO()
            render_route_to_image(lats, lons, image)
            stream_routines.append(save_activity_image(activity_id, image))
    activity = client.get_activity(activity_id)
    await save_activity(activity_id, activity)
    await asyncio.gather(*stream_routines)
    return activity


async def handle_subscription(body: model.SubscriptionUpdate):
    if body.aspect_type == "create":
        logger.info(f"New activity: {body.object_id}")
        athlete_id = body.owner_id
        user_id = await get_user_athlete(athlete_id)
        if not user_id:
            logger.warning(f"Could not find user for athlete {athlete_id}")
            return
        client = await authenticate(user_id)
        await import_user_activity(user_id, body.object_id, client)
    elif (
        body.aspect_type == "update"
        and body.object_type == "activity"
        and "title" in body.updates
    ):
        logger.info(f"Activity title update: {body.object_id}")
        title = body.updates["title"]
        await update_user_activity(body.object_id, title)
    else:
        logger.warning(f"Unhandled subscription update: {body}")


async def strava_recent_activities(user_id: str) -> list[BasicActivity]:
    return await get_activities(user_id)


def format_pace(value: Quantity | float) -> str:
    ms = value.magnitude if isinstance(value, Quantity) else value
    minutes, seconds = divmod(1000 // ms, 60)
    return f"{int(minutes):02d}:{int(seconds):02d} /km"


def format_duration(timedelta: datetime.timedelta | int | float) -> str:
    total_seconds = int(
        timedelta.total_seconds()
        if isinstance(timedelta, datetime.timedelta)
        else timedelta
    )
    hours, remainder = divmod(total_seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    hours, minutes, seconds = int(hours), int(minutes), int(seconds)
    if hours:
        return f"{hours}:{minutes:02d}:{seconds:02d}"
    return f"{minutes}:{seconds:02d}"


def format_distance(meters: Quantity | float) -> str:
    value = meters.magnitude if isinstance(meters, Quantity) else meters
    if value < 1000:
        return f"{value:.0f} m"
    km = value / 1000
    return f"{km:.2f} km"


def format_speed(meters_per_second: Quantity | float) -> str:
    value = (
        meters_per_second.magnitude
        if isinstance(meters_per_second, Quantity)
        else meters_per_second
    )
    kmh = value * 3.6
    return f"{kmh:.2f} km/h"


async def extract_activity_data(activity: Activity) -> ActivityData:
    relevant_fields = {
        "average_speed": {"name": "Speed", "value": format_speed},
        "distance": {"name": "Distance", "value": format_distance},
        "max_speed": {"name": "Max Speed", "value": format_speed},
        "moving_time": {"name": "Time", "value": format_duration},
        "average_heartrate": {
            "name": "Avg HR",
            "value": lambda v: f"{v:.0f} bpm",
        },
    }
    if activity.type == "Run":
        relevant_fields["average_speed"] = {"name": "Pace", "value": format_pace}
        relevant_fields["max_speed"] = {"name": "Max Pace", "value": format_pace}

    stats = []
    for field, meta in relevant_fields.items():
        value = getattr(activity, field, None)
        if value:
            try:
                stats.append(
                    Stat(
                        name=meta["name"],
                        value=meta["value"](value),
                    )
                )
            except Exception as e:
                logger.error(f"Failed to extract {field} from activity: {e}")
    return ActivityData(statistics=stats)


def split_by_indices(
    indices: tuple[int, int], streams: dict[str, list[float | int]]
) -> dict[str, list[float | int]]:
    return {key: value[indices[0] : indices[1]] for key, value in streams.items()}


def analyse_split(streams: dict[str, list[float | int]]) -> dict[str, float]:
    # streams: altitude, distance, heartrate, time, velocity_smooth
    stats: dict[str, float] = {
        "distance": streams["distance"][-1] - streams["distance"][0],
        "duration": streams["time"][-1] - streams["time"][0],
    }
    stats["average_speed"] = stats["distance"] / stats["duration"]
    stats["max_speed"] = max(streams["velocity_smooth"])
    if "heartrate" in streams:
        stats["average_heartrate"] = sum(streams["heartrate"]) / len(
            streams["heartrate"]
        )
    return stats


def unit_filter(value: float, name: str, activity_type: str):
    match name:
        case "distance":
            return format_distance(value)
        case "duration":
            return format_duration(value)
        case "average_speed":
            if activity_type == "Run":
                return format_pace(value)
            return format_speed(value)
        case "max_speed":
            if activity_type == "Run":
                return format_pace(value)
            return format_speed(value)
        case "average_heartrate":
            return f"{value:.0f} bpm"
        case _:
            return f"{value:.2f}"


def unit_name_filter(name: str, activity_type: str):
    match name:
        case "distance":
            return "Dist"
        case "duration":
            return "Dur"
        case "average_speed":
            if activity_type == "Run":
                return "Pace"
            return "Speed"
        case "max_speed":
            if activity_type == "Run":
                return "M Pace"
            return "M Speed"
        case "average_heartrate":
            return "Avg HR"
        case _:
            return name
