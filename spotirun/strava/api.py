import json
import logging
import os

from fastapi import APIRouter, Depends, Request, Response, BackgroundTasks
from fastapi.responses import RedirectResponse
from stravalib import Client

from spotirun.auth import current_user, Auth0User
from spotirun.strava.auth import authenticate
from spotirun.strava.core import import_user_activity, handle_subscription

router = APIRouter()

strava_verify_token = os.environ.get("STRAVA_VERIFY_TOKEN")

logger = logging.getLogger(__name__)


@router.get("/activities")
async def strava_recent_activities(user: Auth0User = Depends(current_user)):
    client = await authenticate(user.id)
    response = client.get_activities(limit=10)
    activities = []
    for activity in response:
        activities.append(
            {
                "id": activity.id,
                "distance": activity.distance,
                "elapsed_time": activity.elapsed_time,
                "type": activity.type,
                "start_date": activity.start_date,
            }
        )
    return activities


@router.post("/activities/{activity_id}")
@router.get("/activities/{activity_id}/import")
async def strava_import_activity(
    request: Request, activity_id: int, user: Auth0User = Depends(current_user)
):
    client = await authenticate(user.id)
    await import_user_activity(user.id, activity_id, client)
    return RedirectResponse(url=request.url_for("analytics", activity_id=activity_id))


@router.get("/activities/{activity_id}")
async def strava_get_activity(
    activity_id: int, user: Auth0User = Depends(current_user)
):
    client = await authenticate(user.id)
    activity = client.get_activity(activity_id)
    return activity


@router.post("/webhook")
async def strava_webhook(request: Request, background_tasks: BackgroundTasks):
    client = Client()
    body = await request.body()
    body = json.loads(body)
    res = client.handle_subscription_update(body)
    background_tasks.add_task(handle_subscription, res)


@router.get("/webhook")
async def strava_webhook_verify(request: Request):
    challenge = request.query_params.get("hub.challenge")
    verify_token = request.query_params.get("hub.verify_token")
    if verify_token != strava_verify_token:
        return Response(status_code=403, content="Invalid verify token")
    return {"hub.challenge": challenge}
