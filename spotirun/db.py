import json
import os

import redis
from databases import Database
from pydantic import BaseModel

data_path = os.environ.get("SPOTIRUN_DATA_PATH", "data")
os.makedirs(data_path, exist_ok=True)
sqlite_path = os.path.join(data_path, "spotirun.sqlite")
db = Database(f"sqlite+aiosqlite:///{sqlite_path}")


redis_host = os.environ.get("SPOTIRUN_REDIS_HOST", "localhost")
redis_db = redis.Redis(host=redis_host, port=6379, db=0)


class Analysis(BaseModel):
    facts: list[str]
    tags: list[str]
    title: str


async def get_shared_token(activity_id: int) -> str | None:
    return await db.fetch_val(
        "SELECT token FROM shared_activities WHERE activity_id=:activity_id",
        {"activity_id": activity_id},
    )


async def save_shared_token(activity_id: int, user_id: str, token: str) -> None:
    await db.execute(
        "INSERT INTO shared_activities (activity_id, user_id, token) VALUES (:activity_id, :user_id, :token) "
        "ON CONFLICT (activity_id) DO UPDATE SET token = excluded.token",
        {"activity_id": activity_id, "token": token, "user_id": user_id},
    )


async def get_shared_activity(token: str) -> tuple[str, int] | None:
    return await db.fetch_one(
        "SELECT user_id, activity_id FROM shared_activities WHERE token=:token",
        {"token": token},
    )


async def save_songs_analysis(activity_id: int, analysis: Analysis) -> None:
    redis_db.set(f"spotirun:activity:{activity_id}:tags", json.dumps(analysis.tags))
    redis_db.set(f"spotirun:activity:{activity_id}:facts", json.dumps(analysis.facts))
    redis_db.set(f"spotirun:activity:{activity_id}:title", json.dumps(analysis.title))


async def load_songs_analysis(activity_id: int) -> Analysis | None:
    tags = redis_db.get(f"spotirun:activity:{activity_id}:tags")
    facts = redis_db.get(f"spotirun:activity:{activity_id}:facts")
    title = redis_db.get(f"spotirun:activity:{activity_id}:title")
    if tags is None or facts is None or title is None:
        return None
    return Analysis(
        tags=json.loads(tags), facts=json.loads(facts), title=json.loads(title)
    )


async def get_tags_for_activities(activity_ids: list[int]) -> dict[int, list[str]]:
    tags = {}
    for activity_id in activity_ids:
        activity_tags = redis_db.get(f"spotirun:activity:{activity_id}:tags")
        if activity_tags:
            tags[activity_id] = json.loads(activity_tags)
    return tags
