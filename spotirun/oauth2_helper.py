import enum
import json
import logging
from typing import TypedDict

from spotirun.db import db as sqlite_db, redis_db

logger = logging.getLogger(__name__)


state_prefix = "spotirun:state:"


def set_state_for_user(user_id: str, state: str):
    redis_db.setex(f"{state_prefix}{state}", 600, user_id)


def get_user_id_from_state(state: str) -> str:
    user_id = redis_db.get(f"{state_prefix}{state}")
    if type(user_id) is bytes:
        user_id = user_id.decode("utf-8")
    return user_id


class Applications(enum.Enum):
    strava = "strava"
    spotify = "spotify"


class Token(TypedDict):
    access_token: str
    refresh_token: str
    expires_at: int


async def add_token(app: Applications, user_id: str, token: Token):
    logger.info(f"Adding token for user {user_id} for app {app.value}")
    await sqlite_db.execute(
        "INSERT INTO tokens (app, user_id, access_token, refresh_token, expires_at) "
        "VALUES (:app, :user_id, :access_token, :refresh_token, :expires_at)"
        "ON CONFLICT (app, user_id) DO UPDATE SET "
        "access_token=excluded.access_token, refresh_token=excluded.refresh_token, expires_at=excluded.expires_at",
        {
            "app": app.value,
            "user_id": user_id,
            "access_token": token["access_token"],
            "refresh_token": token["refresh_token"],
            "expires_at": token["expires_at"],
        },
    )


async def delete_token(app: Applications, user_id: str):
    logger.info(f"Deleting token for user {user_id} for app {app.value}")
    await sqlite_db.execute(
        "DELETE FROM tokens WHERE app = :app AND user_id = :user_id",
        {"app": app.value, "user_id": user_id},
    )


async def get_token(app: Applications, user_id: str) -> Token | None:
    row = await sqlite_db.fetch_one(
        "SELECT access_token, refresh_token, expires_at FROM tokens WHERE app = :app AND user_id = :user_id",
        {"app": app.value, "user_id": user_id},
    )
    if row is None:
        return None
    return Token(
        access_token=row["access_token"],
        refresh_token=row["refresh_token"],
        expires_at=row["expires_at"],
    )


async def get_users(app: Applications) -> list[str]:
    rows = await sqlite_db.fetch_all(
        "SELECT user_id FROM tokens WHERE app = :app", {"app": app.value}
    )
    return [row["user_id"] for row in rows]


async def add_user_info(app: Applications, user_id: str, user_info: dict):
    await sqlite_db.execute(
        "INSERT INTO user_info (app, user_id, user_info) VALUES (:app, :user_id, :user_info)"
        "ON CONFLICT (app, user_id) DO UPDATE SET user_info=excluded.user_info",
        {
            "app": app.value,
            "user_id": user_id,
            "user_info": json.dumps(user_info, default=str),
        },
    )


async def get_user_info(app: Applications, user_id: str) -> dict | None:
    row = await sqlite_db.fetch_one(
        "SELECT user_info FROM user_info WHERE app = :app AND user_id = :user_id",
        {"app": app.value, "user_id": user_id},
    )
    if row is None:
        return None
    return json.loads(row["user_info"])


async def delete_user_info(app: Applications, user_id: str):
    await sqlite_db.execute(
        "DELETE FROM user_info WHERE app = :app AND user_id = :user_id",
        {"app": app.value, "user_id": user_id},
    )
