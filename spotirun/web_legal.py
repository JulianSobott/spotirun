import logging

from fastapi import APIRouter, Request
from fastapi.templating import Jinja2Templates

router = APIRouter()


templates = Jinja2Templates(directory="templates")

logger = logging.getLogger(__name__)


@router.get("/support")
async def support(request: Request):
    return templates.TemplateResponse(request=request, name="legal/support.html.j2")


@router.get("/datenschutz")
async def support(request: Request):
    return templates.TemplateResponse(request=request, name="legal/datenschutz.html.j2")


@router.get("/impressum")
async def support(request: Request):
    return templates.TemplateResponse(request=request, name="legal/impressum.html.j2")
