import logging
import os

from fastapi import FastAPI
from fastapi.templating import Jinja2Templates
from starlette.middleware.sessions import SessionMiddleware
from starlette.requests import Request
from starlette.staticfiles import StaticFiles

from spotirun.auth import router as auth_router, setup_oauth
from spotirun.frontend import router as frontend_router
from spotirun.spotify import router as spotify_router
from spotirun.strava.api import router as strava_api_router
from spotirun.strava.auth import router as strava_auth_router
from spotirun.web_legal import router as web_legal_router

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)

logger = logging.getLogger(__name__)

app = FastAPI()

setup_oauth()


app.add_middleware(SessionMiddleware, secret_key=os.environ["SPOTIRUN_SECRET_KEY"])

app.include_router(auth_router, prefix="/auth", tags=["auth"])
app.include_router(
    spotify_router,
    prefix="/spotify",
    tags=["spotify"],
)
app.include_router(
    strava_auth_router,
    prefix="/strava",
    tags=["strava"],
)
app.include_router(
    strava_api_router,
    prefix="/strava",
    tags=["strava"],
)
app.include_router(
    frontend_router,
    tags=["frontend"],
)
app.include_router(
    web_legal_router,
    tags=["legal"],
)


templates = Jinja2Templates(directory="templates")


@app.get("/")
async def index(request: Request):
    return templates.TemplateResponse(
        request=request,
        name="index.html.j2",
    )


app.mount("/", StaticFiles(directory="static", html=True), name="static")
